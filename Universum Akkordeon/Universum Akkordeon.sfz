// Universum Piano Akkordeon with 48 Buttons
//
// Recorded 2022-01-11, 48 kHz/24-bit, using a Røde NT-1A into a Focusrite Scarlett 6i6
//
// Recording, sample editing and SFZ by Christopher Arndt
//
//

<global>
    // Samples are not looped and sustain for at least 8 seconds
    loop_mode=no_loop
    volume=-6

    // Simple amp EG with medium velocity sensitivity
    amp_veltrack=40
    ampeg_attack=0.04
    ampeg_release=0.08

    // Filter cutoff on CC #74
    fil_type=lpf_2p
    cutoff=20000
    cutoff_cc74=-4800

    // Vibrato on General Purpose CC #17
    pitchlfo_depthcc17=24
    pitchlfo_freq=6.0

    // Slow attack with modwheel down for beginning of phrase
    // Raise modwheel for faster runs
    offset=0
    offset_cc1=5500

<group>
    lokey=59
    pitch_keycenter=59
    hikey=59

<region>
    sample=samples/Universum Akkordeon B2.flac
    tune=-2

<group>
    lokey=60
    pitch_keycenter=60
    hikey=60

<region>
    sample=samples/Universum Akkordeon C3.flac
    tune=-7


<group>
    lokey=61
    pitch_keycenter=61
    hikey=61

<region>
    sample=samples/Universum Akkordeon C#3.flac
    tune=-7


<group>
    lokey=62
    pitch_keycenter=62
    hikey=62

<region>
    sample=samples/Universum Akkordeon D3.flac
    tune=-9


<group>
    lokey=63
    pitch_keycenter=63
    hikey=63

<region>
    sample=samples/Universum Akkordeon D#3.flac
    tune=-8


<group>
    lokey=64
    pitch_keycenter=64
    hikey=64

<region>
    sample=samples/Universum Akkordeon E3.flac
    tune=-6


<group>
    lokey=65
    pitch_keycenter=65
    hikey=65

<region>
    sample=samples/Universum Akkordeon F3.flac
    tune=-10


<group>
    lokey=66
    pitch_keycenter=66
    hikey=66

<region>
    sample=samples/Universum Akkordeon F#3.flac
    tune=-6


<group>
    lokey=67
    pitch_keycenter=67
    hikey=67

<region>
    sample=samples/Universum Akkordeon G3.flac
    tune=-7


<group>
    lokey=68
    pitch_keycenter=68
    hikey=68

<region>
    sample=samples/Universum Akkordeon G#3.flac
    tune=-7


<group>
    lokey=69
    pitch_keycenter=69
    hikey=69

<region>
    sample=samples/Universum Akkordeon A3.flac
    tune=-6


<group>
    lokey=70
    pitch_keycenter=70
    hikey=70

<region>
    sample=samples/Universum Akkordeon A#3.flac
    tune=-12


// B3 sample has an audible pop righ at the start of note :(
//<group>
//    lokey=71
//    pitch_keycenter=71
//    hikey=71

//<region>
//    sample=samples/Universum Akkordeon B3.flac
//    tune=-5


<group>
    lokey=71
    pitch_keycenter=72
    hikey=72

<region>
    sample=samples/Universum Akkordeon C4.flac
    tune=-11


<group>
    lokey=73
    pitch_keycenter=73
    hikey=73

<region>
    sample=samples/Universum Akkordeon C#4.flac
    tune=-7


<group>
    lokey=74
    pitch_keycenter=74
    hikey=74

<region>
    sample=samples/Universum Akkordeon D4.flac
    tune=-6


<group>
    lokey=75
    pitch_keycenter=75
    hikey=75

<region>
    sample=samples/Universum Akkordeon D#4.flac
    tune=-5


<group>
    lokey=76
    pitch_keycenter=76
    hikey=76

<region>
    sample=samples/Universum Akkordeon E4.flac
    tune=-5


<group>
    lokey=77
    pitch_keycenter=77
    hikey=77

<region>
    sample=samples/Universum Akkordeon F4.flac
    tune=-16


<group>
    lokey=78
    pitch_keycenter=78
    hikey=78

<region>
    sample=samples/Universum Akkordeon F#4.flac
    tune=-9


<group>
    lokey=79
    pitch_keycenter=79
    hikey=79

<region>
    sample=samples/Universum Akkordeon G4.flac
    tune=-13


<group>
    lokey=80
    pitch_keycenter=80
    hikey=80

<region>
    sample=samples/Universum Akkordeon G#4.flac
    tune=-8


<group>
    lokey=81
    pitch_keycenter=81
    hikey=81

<region>
    sample=samples/Universum Akkordeon A4.flac
    tune=-12


<group>
    lokey=82
    pitch_keycenter=82
    hikey=82

<region>
    sample=samples/Universum Akkordeon A#4.flac
    tune=-10


<group>
    lokey=83
    pitch_keycenter=83
    hikey=83

<region>
    sample=samples/Universum Akkordeon B4.flac
    tune=-9


// Stretch hightest sample (C5) up to E5
<group>
    lokey=84
    pitch_keycenter=84
    hikey=88

<region>
    sample=samples/Universum Akkordeon C5.flac
    tune=12
