# The Liberatron Library

An open sample library in [SFZ] format of keyboard sounds in the style of the
[Mellotron].

The library comprises these instruments so far:

* [Hohner Melodica Piano 26](./Hohner%20Melodica%20Piano%2026)
* [Lothar Gaerthner Kantele](./Lothar%20Gaerthner%20Kantele)
* [Setanta Low D Whistle](./Setanta%20Low%20D%20Whistle)
* [Universum Akkordeon](./Universum%20Akkordeon)


## Project Goals

Provide an extensive and growing collection of sample instrument libraries using
original samples *in the spirit* of the original Mellotron sounds, created by
the community and offered with an open and free license.

While it is hard to exactly to define what creates the magic of the original
Mellotron sounds, most share some common characteristics, which are also 
desirable for for this library:

* Natural acoustic or electro-acoustic instruments or human voice
* Long decay or (potentially) endless sustain
* Instant or short attack
* Suitable for playing on a keyboard
* Suitable for playing melodies and harmonies or chords
* Blending well with other sounds
* Not being too polished and having some kind of *human touch*

In addition, the Mellotron libraries include short musical phrases and loops
and effects also using sounds, which do not fit neatly in the above criteria
(e.g. percussion loops, vamps, orchestra effects etc.). Sample sets in the
same vein and style will also have a place in this collection.


## Sample Guidelines

* One sample per note **a.**
* A range of at least two octaves (e.g. C3 to C5); if possible, 35 notes from G2
  to F5 **b.**
* One velocity layer, preferably mezzoforte
* Un-looped, 7 - 8 seconds per sample with natural attack and release **c.**
* Mostly flat dynamic expression over the whole length of each sample
* Moderate use of natural vibrato (unless as a desired effect)
* Retain imperfections and intonation variations.
* Low-fi and vintage effects should not be "baked" into the samples or, if they
  are, only provided as an alternative to the unprocessed samples.

Notes:

**a.** For diatonic instruments, sample regions may be stretched to include the
note one semitone above and/or below, if needed.

**b.** For instruments with a limited range, either combine it with an 
instrument in the same family but a different register (e.g. soprano and alto
recorder), or stretch the lowest and highest sample to cover a few more 
semitones.

**c.** For instruments or sounds with shorter decay (e.g. plucked instruments
or percussion) or where other factors (e.g. running out of breath in higher
registers of woodwind or brass instruments) make it difficult to achieve such a 
long sustain, shorter samples are ok. Alternatively, record a musical phrase or
use a sustaining playing technique (e.g. tremolo) or an ensemble.


## Author

Recording, sample editing and SFZ authoring by *Christopher Arndt*


## Contributing

This project is open for contributions, for example:

* Corrections and additions to SFZ files
* Alternative SFZ files for existing sample sets
* New sample sets (even raw uncut recording sessions are welcome)
* New instruments

Please use the issue tracker to discuss or submit your contribution.


## License

The samples and libraries are published under the CC0 1.0 Universal license.

This means you are free to download and use these samples in your projects 
without attribution and share them with others or create derivative works
in any way you see fit and share those too.


## Further Mellotron Ressources

* [Mellotron.com](http://www.mellotron.com/)

    US manufacturers and trademark owners

* [Streetly Electronics](http://mellotronics.com/)

    Original UK manufacturer of the Mellotron

* Out Of Phase. Magic Machines Resources: [Melltron pages](https://www.outofphase.fr/introduction-mellotron/)

    French web page with information about the Mellotron. Has PDFs of the original 
    sales brochures and owner's manuals, audio extracts of Mellotron music, videos
    and much more.

* Norm Leete: [Mellotron Info](http://www.normleete.co.uk/?page_id=6)

    History, description of the different models, how it works, maintenance


[SFZ]: https://sfzformat.com
[Mellotron]: https://www.mellotron.com/
